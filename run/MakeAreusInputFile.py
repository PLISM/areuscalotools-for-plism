#!/usr/bin/env athena.py
# -*- coding: utf-8 -*-

"""Convert `.HITS.` samples into NTuple files for use in AREUS.

This ATHENA script accepts the output of a Geant4 simulation, extracts
hit information and writes it out in a D3PD format that can be read by
AREUS.

This script accepts the following arguments:

    infile  : The input file name. This is passed to the Python
              function `glob.glob`, so any wildcard character in it
              will get expanded. You may also pass a Python `list` of
              file names -- in that case, no expansion will occur.
    outfile : The output file name. If omitted, the default value
              "ntuple.AREUS.root" is used.

Arguments may be passed as follows:

    athena MakeAreusInputFile.py -c 'infile="a.root"; outfile="b.root"'

Equally valid are:

    athena MakeAreusInputFile.py -c 'infile=["1.root", "2.root"]'
    athena MakeAreusInputFile.py -c '
    input = "SignalSampleHITSFiles/*.root"
    output = "MyLongSampleName.AREUS.root"
    '

Since the -c option accepts arbitrary Python code, the argument-passing
logic may be as simple or complicated as you want.
"""

#--------------------------------------------------------------
# Check command-line arguments
#--------------------------------------------------------------

if 'infile' not in vars():
    infile = None
vars().setdefault('outfile', 'ntuple.AREUS.root')

#--------------------------------------------------------------
# General settings
#--------------------------------------------------------------

from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr

theApp.EvtMax = -1
ServiceMgr.MessageSvc.OutputLevel = WARNING

#--------------------------------------------------------------
# Load POOL support
#--------------------------------------------------------------

from AthenaPoolCnvSvc.AthenaPoolCnvSvcConf import AthenaPoolCnvSvc
from CLIDComps.CLIDCompsConf import ClassIDSvc

# Warning: This import actually executes code!
from AthenaPoolCnvSvc import ReadAthenaPool as _

ServiceMgr += AthenaPoolCnvSvc()
ServiceMgr.AthenaPoolCnvSvc.PoolAttributes = ["DEFAULT_BUFFERSIZE = '2048'"]
ServiceMgr.PoolSvc.AttemptCatalogPatch = True

ServiceMgr += ClassIDSvc()
include('PartPropSvc/PartPropSvc.py')

#--------------------------------------------------------------
# Setup the input file
#--------------------------------------------------------------

import glob

if infile is not None:
    if isinstance(infile, str):
        ServiceMgr.EventSelector.InputCollections = glob.glob(infile)
    else:
        ServiceMgr.EventSelector.InputCollections = infile

ServiceMgr.EventSelector.SkipEvents = 0

#--------------------------------------------------------------
# McEventCollection to xAOD::TruthEventContainer conversion for athena >= 20.
#--------------------------------------------------------------

try:
    from AthenaCommon.AlgSequence import AlgSequence
    theJob = AlgSequence()

    # Run the xAOD truth builder
    from xAODTruthCnv.xAODTruthCnvConf import xAODMaker__xAODTruthCnvAlg
    alg1 = xAODMaker__xAODTruthCnvAlg()
    alg1.AODContainerName = "TruthEvent"
    theJob += alg1
except ImportError:
    pass

#--------------------------------------------------------------
# Setup the output file
#--------------------------------------------------------------

from OutputStreamAthenaPool.MultipleStreamManager import MSMgr

alg = MSMgr.NewRootStream('StreamNTUP_GEN', outfile, 'gend3pd')

#--------------------------------------------------------------
# Setup geometry so we can convert to global coordinates
#--------------------------------------------------------------

from AthenaCommon.GlobalFlags import globalflags

# load all possible converters for EventCheck
GeoModelSvc = Service('GeoModelSvc')
GeoModelSvc.IgnoreTagDifference=True

# set up all detector description stuff
include('RecExCond/AllDet_detDescr.py')

# Conditions and geometry tags for Phase-I studies.
#~ globalflags.DetDescrVersion = 'ATLAS-GEO-20-00-01'
#~ globalflags.ConditionsTag = 'OFLCOND-MC12-SIM-00'

# Conditions and geometry tags for Phase-II studies.
globalflags.DetDescrVersion = 'ATLAS-P2-ITK-13-00-02'
globalflags.ConditionsTag = 'OFLCOND-MC15c-SDR-13'

#--------------------------------------------------------------
# Define LAr and Tile hit D3PD objects to be written out
#--------------------------------------------------------------

import AreusCaloTools
import CaloD3PDMaker
import D3PDMakerCoreComps
from D3PDMakerConfig.D3PDMakerFlags import D3PDMakerFlags
from D3PDMakerCoreComps.D3PDObject import D3PDObject

def make_lar_d3pd_object(name, prefix, **kwargs):
    """Maker callback function for the creation of LArHitD3PDObjects."""
    getter = CaloD3PDMaker.LArHitContainerGetterTool(
        name+'_Getter',
        TypeName='LArHitContainer',
        SGKey=name,
        Label=prefix,
        )
    return D3PDMakerCoreComps.VectorFillerTool(
        name,
        Prefix=prefix,
        Getter=getter,
        ObjectName=kwargs.get('object_name', name+'D3PDObject'),
        SaveMetadata=D3PDMakerFlags.SaveObjectMetadata(),
        )

def make_tile_d3pd_object(name, prefix, **kwargs):
    """Maker callback function for the creation of TileHitD3PDObjects."""
    getter = CaloD3PDMaker.SGTileHitGetterTool(
        name+'_Getter',
        TypeName='TileHitVector',
        SGKey='TileHitVec',
        Label=prefix,
        )
    return D3PDMakerCoreComps.VectorFillerTool(
        name,
        Prefix=prefix,
        Getter=getter,
        ObjectName=kwargs.get('object_name', name+'D3PDObject'),
        SaveMetadata=D3PDMakerFlags.SaveObjectMetadata(),
        )

lar_d3pd_object = D3PDObject(make_lar_d3pd_object)
lar_d3pd_object.defineBlock(1, 'Hits', AreusCaloTools.LArHitFillerTool)

tile_d3pd_object = D3PDObject(make_tile_d3pd_object)
tile_d3pd_object.defineBlock(1, 'TileHitDetails', AreusCaloTools.TileHitFillerTool)

for component in ['emb', 'emec', 'hec', 'fcal']:
    name = 'LArHit' + component.upper()
    alg += lar_d3pd_object(1, name=name, prefix='hit{0}_'.format(component))
alg += tile_d3pd_object(1, name='TileHit', prefix='hittile_')

#--------------------------------------------------------------
# Define MC Truth D3PD objects to be written out
#--------------------------------------------------------------

from TruthD3PDMaker.GenParticleD3PDObject import GenParticleD3PDObject

alg += GenParticleD3PDObject( 10, prefix='mc_', sgkey='TruthEvent')

#--------------------------------------------------------------
# Define MC Truth Jet D3PD objects to be written out
#--------------------------------------------------------------

# Special-casing because `make_StandardJetGetter` got removed in
# ATHENA 20.
try:
    # ATHENA >=20.
    from JetD3PDMaker.JetD3PDObject import JetD3PDObject
    from JetRec.JetRecFlags import jetFlags
    from JetRec.JetAlgorithm import addJetRecoToAlgSequence

    jetFlags.useTopo = False
    jetFlags.useTracks = False

    from JetRec.JetRecStandardToolManager import jtm

    jtm.addJetFinder('AntiKt4TruthJets', 'AntiKt', 0.4, 'truth', ptmin=7*GeV)
    addJetRecoToAlgSequence()
except ImportError:
    # ATHENA <20 fallback.
    from D3PDMakerConfig.JetD3PDObject import JetD3PDObject
    from JetRec.JetGetters import make_StandardJetGetter

    make_StandardJetGetter(
        'AntiKt', 0.4, 'Truth',
        minPt=7*GeV,
        useInteractingOnly=True,
        outputCollectionName='AntiKt4TruthJets',
        )

alg += JetD3PDObject(0, prefix='AntiKt4TruthJets_', sgkey='AntiKt4TruthJets')
